package com.educo.player;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;

import com.educo.player.widget.CommonDialog;

import java.util.ArrayList;
import java.util.List;

import kr.imgtech.lib.zoneplayer.IMGApplication;
import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;
import kr.imgtech.lib.zoneplayer.util.ConfigurationManager;

/**
 * 인트로 화면
 */
public class IntroActivity extends AppCompatActivity implements BaseInterface, BaseDialogListener {

    // 저장소 저장 권한 코드
    private final int MY_PERMISSIONS_REQUEST = 1;

    private List<Runnable> queue = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);

        checkPermissionOverlay();
    }

    @Override
    protected void onResume() {
        super.onResume();

        for (Runnable work : queue) {

            (new Thread(work)).start();
        }
        queue.clear();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case IMGApplication.REQ_CODE_OVERLAY_PERMISSION:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.canDrawOverlays(getApplicationContext())) {

                        startIntroAsyncTask();
                    } else {

                        queue.add(new Runnable() {

                            @Override
                            public void run() {
                                dialogPermissionOverlayInfo();
                            }
                        });
                    }
                }
                break;

            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDialog(int state, int code, int result) {

        if (state == DIALOG_PERMISSION_OVERLAY_INFO) {

            // 오버레이 권한 알림에서 다시 허용을 위해 확인 선택
            if (result == CommonDialog.DIALOG_CHOICE_POSITIVE) {
                checkPermissionOverlay();

                // 알림에서 허용을 다시 하지 않도록 취소 선택
            } else {

                startIntroAsyncTask();
            }

        } else if (state == DIALOG_PERMISSION_NEED) {

            startIntroAsyncTask();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST:

                // 1개의 권한이라도 비허용이면 false
                boolean allOK = true;   // 권한 전체 허용 여부를 우선 true 초기화
                for (int result : grantResults) {
                    if (result == PackageManager.PERMISSION_DENIED) {
                        allOK = false;  // 권한 비허용
                        break;          // 비허용 반환
                    }
                }

                if ((grantResults.length > 0) && (allOK)) {

                    startIntroAsyncTask();

                }

                break;
        }
    }

    /**
     * 오버레이 권한 체크
     */
    public void checkPermissionOverlay() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (! Settings.canDrawOverlays(getApplicationContext())) {
                startPermissionOverlayWindow();
            } else {
                startIntroAsyncTask();
            }
        } else {
            startIntroAsyncTask();
        }
    }

    /**
     * 오버레이 권한 화면 표출
     */
    @TargetApi(Build.VERSION_CODES.M)
    public void startPermissionOverlayWindow() {

        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, IMGApplication.REQ_CODE_OVERLAY_PERMISSION);
    }

    /**
     * 오버레이 권한 획득 후 메인 화면 전환 실행
     */
    public void startIntroAsyncTask() {

        new IntroAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * 메인 화면 전환
     */
    private void startMainActivity() {

        finish();
    }

    /**
     * 오버레이 권한 알림 표출
     */
    private void dialogPermissionOverlayInfo() {

        CommonDialog dialog = CommonDialog.getInstance(DIALOG_PERMISSION_OVERLAY_INFO, this);
        dialog.show(getSupportFragmentManager(), CommonDialog.DIALOG_TAG);
    }

    /**
     * 인트로 처리 후 메인 화면 전환 AsyncTask
     */
    private class IntroAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // M 버전은 절대 통합 플레이어로 설정
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ConfigurationManager.setPlayerMode(getApplicationContext(), 3);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            startMainActivity();
        }

    }
}
