package com.educo.player.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.educo.player.R;

import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;

/**
 * DialogFragment 구현 Download 및 기본 화면 Dialog
 * @author kimsanghwan
 * @since 2014. 9. 13.
 */
public class CommonDialog extends DialogFragment implements BaseInterface {

    public final static String DIALOG_TAG = "ZONEPLAYER_DIALOG";
    public final static int DIALOG_CHOICE_POSITIVE = 1;			// 확인 등 실행의 선택
    public final static int DIALOG_CHOICE_NEUTRAL = 0;			// 중립 선택
    public final static int DIALOG_CHOICE_NEGATIVE = -1;		// 아니오 등 거부의 선택
    public final static int DIALOG_CANCEL = 2;					// 버튼 선택없이 백버튼으로 취소

    private final static String KEY_STATE = "state";			// 다이얼로그 상태키
    private final static String KEY_CODE = "code";				// 표출 코드키
    private final static String KEY_MESSAGE = "message";		// 표출 메시지 키

    private int state;									// 다이얼로그 상태
    private int code;									// 전달된 코드
    private String message;								// 전달된 메시지
    private BaseDialogListener listener;				// 다이얼로그 선택을 받는 리스너

    /**
     * 다이얼로그 생성 및 반환
     * @param state	다이얼로그 상태
     * @return	DownloadDialog
     */
    public static CommonDialog getInstance(int state) {
        CommonDialog dialog = new CommonDialog();

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);

        dialog.setArguments(args);

        return dialog;
    }

    /**
     * 다이얼로그 생성
     * @param state			다이얼로그 상태
     * @param listener		BaseDialogListener
     * @return	DownloadDialog
     */
    public static CommonDialog getInstance(int state, BaseDialogListener listener) {
        CommonDialog dialog = new CommonDialog();

        dialog.listener = listener;

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);

        dialog.setArguments(args);

        return dialog;
    }

    /**
     * 다이얼로그 생성
     * 다이얼로그 화면에 특정 코드를 동시 표출
     * @param state			다이얼로그 상태
     * @param code			특정 코드
     * @param listener		BaseDialogListener
     * @return	DownloadDialog
     */
    public static CommonDialog getInstance(int state, int code, BaseDialogListener listener) {
        CommonDialog dialog = new CommonDialog();

        dialog.listener = listener;

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);
        args.putInt(KEY_CODE, code);

        dialog.setArguments(args);

        return dialog;
    }

    /**
     * 다이얼로그 생성
     * 다이얼로그 화면에 특정 코드 및 메시지 동시 표출
     * @param state		다이얼로그 상태
     * @param code		특정 코드
     * @param message	특정 메시지
     * @param listener	BaseDialogListener
     * @return	DownloadDialog
     */
    public static CommonDialog getInstance(int state, int code, String message, BaseDialogListener listener) {
        CommonDialog dialog = new CommonDialog();

        dialog.listener = listener;

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);
        args.putInt(KEY_CODE, code);
        args.putString(KEY_MESSAGE, message);

        dialog.setArguments(args);

        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Bundle arguments = getArguments();

        state = arguments.getInt(KEY_STATE);

        if (arguments.containsKey(KEY_CODE)) {	// 코드가 존재하면
            code = arguments.getInt(KEY_CODE);
        }

        if (arguments.containsKey(KEY_MESSAGE)) {	// 메시지가 존재하면
            message = arguments.getString(KEY_MESSAGE);
        }

        switch (state) {

            // 오버레이 권한 누락 알림
            case DIALOG_PERMISSION_OVERLAY_INFO:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(R.string.dialog_permission_overlay);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
                break;

            // 기타 오류
            default:
                builder.setTitle(R.string.dialog_title_common);
                builder.setMessage(getResources().getString(R.string.dialog_message_error_finish));
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                break;
        }

        return builder.create();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * 다이얼로그 표출시 백버튼 터치 이벤트
     * 백버튼 터치 시 이벤트 수신
     */
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        switch (state) {

            default:
                if (listener != null) {
                    listener.onDialog(state, 0, DIALOG_CANCEL);
                }
                break;
        }
    }

}
